*LambdaRuntime
materializeFrom: aDecoder
	"Quick fix to workaround 64 bit image issue.
	see: https://pharo.fogbugz.com/f/cases/20120/Fuel-is-not-64bits-ready"

	^ (BoxedFloat64 new: 2)
		at: 1 put: aDecoder nextEncodedUint32;
		at: 2 put: aDecoder nextEncodedUint32;
		* 1.