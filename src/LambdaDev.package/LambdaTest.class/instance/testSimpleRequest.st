tests
testSimpleRequest
	| testStream |
	
	testStream := WriteStream on: String new.
	
	Lambda new processJSON: '{"we": "love", "Pharo": "Lambda"}' to: testStream.
	
	self assert: (testStream contents beginsWith: '{"outputSpeech') description: 'Should return a valid response:'