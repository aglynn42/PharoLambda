baseline
baseline: spec
	<baseline>

	spec for: #common do: [
		spec configuration: 'ZTimestamp' with: [
			spec 
				versionString: #stable;
				repository: 'http://mc.stfx.eu/Neo' ].
			
		spec baseline: 'AWS' with: [	
			spec repository: 'github://newapplesho/aws-sdk-smalltalk:v1.10/pharo-repository' ].
						
		spec 
			package: 'Lambda' with: [ spec requires: { 'ZTimestamp'. 'AWS' }];
			package: 'LambdaRuntime' with: [  spec requires: { 'Lambda' }];
			package: 'LambdaDev' with: [  spec requires: { 'LambdaRuntime' }].
			
		spec 
			group: 'Core' with: #('Lambda');
			group: 'Dev' with: #('LambdaDev');
			group: 'default' with: #('LambdaRuntime').
			
	].		
